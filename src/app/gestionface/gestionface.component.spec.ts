import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionfaceComponent } from './gestionface.component';

describe('GestionfaceComponent', () => {
  let component: GestionfaceComponent;
  let fixture: ComponentFixture<GestionfaceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionfaceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionfaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
