import { Component, OnInit,Input } from '@angular/core';
import { fabric } from 'fabric';
import { EditorserviceService, HttpservicesService } from 'src/app/core';
import { getLineAndCharacterOfPosition } from 'typescript';


@Component({
  selector: 'app-formclothes',
  templateUrl: './formclothes.component.html',
  styleUrls: ['./formclothes.component.css']
})
export class FormclothesComponent implements OnInit {
  @Input() categorie :any
  
  size={
  s1:false,
  s2:false,
  s3:false,
  s4:false,
  s5:false,
  s6:false,
  s7:false
  }
 
  oui=false
  visible=false
  viewFace1:any
  category=['Vêtement','Affichage','Emballage','Imprimerie','Gadget'];
  price:any
  material:any
  dimension:any
  volet:any
  grammage:any
  pellicule:any
  border:any
  dim_disp:any
 
  quantity:any
  promoprice:any
  details:any
  products: any
  articlename:any
  num_category:any
  data:any;
  shwp=true;
  width=400;
  height=400
  canvas:any;
  vieweditor=false
  hidesavecloth=true
  viewimage:any
  file:any
  viewi:any
  file1:any
  types=["models","produits"]
  typ:any
  constructor(private http:HttpservicesService, private service:EditorserviceService) { }
  @Input() url:any;
  ngOnInit(): void {
    this.http.get().subscribe(
      res=>{
        console.log(res);
      }
    )
   console.log(this.categorie)
  }
  View1(){
    this.visible=true;
    this.oui=!this.oui
  }
  View2(){
    this.visible=false
  }
  watchS1(){
    this.size.s1=true
    //this.size.a1
    console.log( this.size.s1)
  }

  watchS2(){
    this.size.s2=true
    //this.size.a2
    console.log( this.size.s2)
  }

  watchS3(){
    this.size.s3=true
    //this.size.a3
    console.log( this.size.s3)
  }

  watchS4(){
    this.size.s4=true
    //this.size.a4
    console.log( this.size.s4)
  }

  watchS5(){
    this.size.s5=true
    //this.size.a5
    console.log( this.size.s5)
  }

  watchS6(){
    this.size.s6=true
    //this.size.a6
    console.log( this.size.s6)
  }

  Customize=(data:any)=>{
    this.data=data;
    console.log(data)
    this.shwp=false

  }

  UploadImage(event:any){
     this.file= event.target.files[0]
    const reader = new FileReader();
  reader.onload = () => {
   this.viewimage= reader.result;
   };
   
   reader.readAsDataURL(this.file);
    console.log(this.file)
  
  
  }
  Uploade(event:any){
    this.file1= event.target.files[0]
   const reader = new FileReader();
 reader.onload = () => {
 
  this.viewi= reader.result;
  };
  
  reader.readAsDataURL(this.file1);
   console.log(this.file1)
 
 
 }
  getProduct(){
    this.products={

      description:JSON.stringify({
        url:this.viewimage,

        name:this.articlename,
        made_with:this.details,
        price:this.price,
        promo:this.promoprice,
        owner:76,
        category:this.num_category,
        type:"product",
    
      })

    }
    
    for(let item of this.canvas.getObjects()){
     item.set({lockMovementX:true})
     item.set({lockMovementY:true})
     item.set({lockScalingY:true})
     item.set({lockScalingX:true})
    }
    var json = this.canvas.toJSON(['lockMovementX', 'lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY'])
   // this.testcanvas.loadFromJSON(json, this.testcanvas.renderAll.bind(this.testcanvas));
    //this.testcanvas.setHeight(this.height);
   // this.testcanvas.setWidth(this.width);

    Object.assign(this.products,{obj:JSON.stringify(json),width:this.width,height:this.height})

    this.http.add(this.products).subscribe(
      res=>{
        console.log(res)
      },
      err=>{
        console.log(err)
      }
    )
    
  }

 
  changetype(event:any){
    if(event.target.value == "produits"){
      this.typ="produits"
    }
    if(event.target.value == "models"){
      this.typ="model"
    }
  
    console.log(this.typ)
  }
  Editmodels(event:any){
    this.vieweditor=true
    this.hidesavecloth=false
    this.products={
       description:JSON.stringify({
        url:this.viewimage,
        name:this.articlename,
        made_with:this.details,
        price:this.price,
        qty:this.quantity,
        size: this.size,
        material:this.material,
        promo:this.promoprice,
        owner:76,
        category:this.categorie,
        type:this.typ,
    
      })
     
    }
    if(this.viewi){
      Object.assign(this.products,{url2:this.viewi})
    }else{
      Object.assign(this.products,{url2:null})

    }
  }
 

  
}
