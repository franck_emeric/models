import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormgadgetsComponent } from './formgadgets.component';

describe('FormgadgetsComponent', () => {
  let component: FormgadgetsComponent;
  let fixture: ComponentFixture<FormgadgetsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormgadgetsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormgadgetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
