import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormprintedComponent } from './formprinted.component';

describe('FormprintedComponent', () => {
  let component: FormprintedComponent;
  let fixture: ComponentFixture<FormprintedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormprintedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormprintedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
