import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-formprinted',
  templateUrl: './formprinted.component.html',
  styleUrls: ['./formprinted.component.css']
})
export class FormprintedComponent implements OnInit {
  @Input() categorie:any
  oui = false;
  products:any
  visible = false;
  file:any
  viewimage:any
  viewi:any
  file1:any
  vieweditor = false;
  hideprinted = true;
  articlename:any
  price:any
  volet:any
  grammage:any
  pellicule:any
  border:any
  dimension:any=[]
  promoprice:any
  quantity:any
  details:any 
  types = ["models", "produits"];
  typ: any;
  num_category: any;
  constructor() { }

  ngOnInit(): void {
    console.log(this.categorie)
  }

  View1() {
    this.visible = true;
    this.oui = !this.oui;
  }

  View2() {
    this.visible = false;
  }

  UploadImage(event: any) {
    this.file = event.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      this.viewimage = reader.result;
    };

    reader.readAsDataURL(this.file);
    console.log(this.file);
  }

  Uploade(event: any) {
    this.file1 = event.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      this.viewi = reader.result;
    };

    reader.readAsDataURL(this.file1);
    console.log(this.file1);
  }

  changetype(event: any) {
    if (event.target.value == "produits") {
      this.typ = "produits";
    }
    if (event.target.value == "models") {
      this.typ = "models";
    }

    console.log(this.num_category);
  }

  Editmodels(event: any) {
    this.vieweditor = true;
    this.hideprinted = false;
    this.products = {
      description: JSON.stringify({
        url: this.viewimage,
        url2: this.viewi,
        name: this.articlename,
        made_with: this.details,
        price: this.price,
        qty: this.quantity,
        gram: this.grammage,
        pellicule: this.pellicule,
        border: this.border,
        volet: this.pellicule,
        promo: this.promoprice,
        owner: 76,
        category: this.categorie,
        type: this.typ,
      }),
    };

    console.log(JSON.parse(this.products.description));
  }
}
