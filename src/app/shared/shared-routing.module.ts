import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeaderComponent } from '.';
import { BodyComponent } from './layout/body/body.component';
import { ProductsComponent } from './layout/products/products.component';


const routes: Routes = [

  {path:'products', component:ProductsComponent},
  {path:'login', component:ProductsComponent},
  {path:'header', component:HeaderComponent}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SharedRoutingModule { }
